<!--
.. title: Create an awesome shell experience.
.. type: text
-->

<!--
Optimum Shell (c) by Hong Xu

Optimum Shell is licensed under a Creative Commons Attribution-ShareAlike 4.0 International license.

You should have received a copy of the license along with this work. If not,
see <http://creativecommons.org/licenses/by-sa/4.0/>.
-->

This page lists a collection of useful articles that help build an awesome command line experience
on [POSIX systems](https://en.wikipedia.org/wiki/POSIX), such as GNU/Linux, MacOS, cygwin, etc.

## Getting Started

- [A Command Line Primer for Beginners](https://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything)
- [advanced,optional] [Shell Scripting Tutorial](https://www.shellscript.sh/)

## Which Shell to Use?

Unfortunately, this is a very subjective question. Try the following search results.

- [bash vs zsh vs fish on DuckDuckGo](https://duckduckgo.com/?q=bash+vs+zsh+vs+fish)
- [bash vs zsh vs fish on Google](https://www.google.com/search?q=bash+vs+zsh+vs+fish)
- [bash vs zsh vs fish on Bing](https://www.bing.com/search?q=bash+vs+zsh+vs+fish)

## Basic Configuration

These articles offer basic configuration toward building decent command line experiences. It's
highly recommended to read the ones that are useful for your choice of shell.

- [bash] [A ~/.inputrc for Humans](https://www.topbug.net/blog/2017/07/31/inputrc-for-humans/)
- [zsh] [Switching to ZSH](http://zpalexander.com/switching-to-zsh/)
- [fish] [Try out the fish shell](http://www.perfectlyrandom.org/2014/09/21/try-out-the-fish-shell/)

## Improvements

These articles are guidance on improving command line experiences on various aspects. You can select
the ones that interest you to read.

- [MacOS] [Install and Use GNU Command Line Tools on macOS/OS X](https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/)
- [Managing Dotfiles With Git: Get Your Dotfiles Under Control](https://www.foraker.com/blog/get-your-dotfiles-under-control)
- [A Better ls Command](https://www.topbug.net/blog/2016/11/28/a-better-ls-command/)
- [Complementing cd with Autojump](http://www.linux-magazine.com/Issues/2014/166/Command-Line-autojump)
- [Make the less Command More Powerful](https://www.topbug.net/blog/2016/09/27/make-gnu-less-more-powerful/)
- [Truncate Long Matching Lines of Grep: a Solution That Preserves Color](https://www.topbug.net/blog/2016/08/18/truncate-long-matching-lines-of-grep-a-solution-that-preserves-color/)
- [zsh] [How to add fuzzy completion (like Sublime Text palette) to ZSH](https://stackoverflow.com/a/21597860/1150462)
- [zsh] [Restore the Previously Canceled Command in Zsh](https://www.topbug.net/blog/2016/10/03/restore-the-previously-canceled-command-in-zsh/)
- [fish] [Directory History in the Fish Shell](https://codeyarns.com/2015/05/29/directory-history-in-the-fish-shell/)

## Help and Contribution

If you have any questions, or have any interesting articles in mind which you would like to be
listed on this page, please
consider [opening an issue](https://gitlab.com/xuhdev/optimum-shell/issues/new)
or [creating a merge request](https://gitlab.com/xuhdev/optimum-shell/merge_requests/new).
