# Optimum Shell

This is the source code of the website <http://shell.topbug.net>.

## License

Optimum Shell (c) by Hong Xu

Optimum Shell is licensed under a Creative Commons Attribution-ShareAlike 4.0 International license.

You should have received a copy of the license along with this work. If not,
see <http://creativecommons.org/licenses/by-sa/4.0/>.
